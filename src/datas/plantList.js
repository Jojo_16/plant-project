import monstera from '../assets/monstera.jpg'
import lyrata from '../assets/lyrata.jpg'
import pothos from '../assets/pothos.jpg'
import succulent from '../assets/succulent.jpg'
import olivier from '../assets/olivier.jpg'
import basil from '../assets/basil.jpg'
import mint from '../assets/mint.jpg'
import calathea from '../assets/calathea.jpg'
import cactus from '../assets/cactus.jpg'

export const plantList = [
	{
		name: 'monstera',
		category: 'typClass',
		id: '1ed',
		light: 2,
		water: 3,
		price: 10,
		cover: monstera,
		range: 2
	},
	{
		name: 'ficus lyrata',
		category: 'typClass',
		id: '2ab',
		light: 3,
		water: 1,
		price: 11,
		cover: lyrata,
		range: 3
	},

	{
		name: 'pothos argenté',
		category: 'typClass',
		id: '3sd',
		light: 1,
		water: 2,
		price: 4,
		cover: pothos,
		range: 3
	},
	{
		name: 'calathea',
		category: 'typClass',
		id: '4kk',
		light: 2,
		water: 3,
		price: 7,
		cover: calathea,
		range: 3
	},
	{
		name: 'olivier',
		category: 'typOut',
		id: '5pl',
		light: 3,
		water: 1,
		price: 9,
		cover: olivier,
		range: 2
	},

	{
		name: 'cactus',
		category: 'typGreas',
		id: '8fp',
		light: 2,
		water: 1,
		price: 13,
		cover: cactus,
		range: 3
	},
	{
		name: 'basilique',
		category: 'typOut',
		id: '7ie',
		light: 2,
		water: 3,
		price: 16,
		cover: basil,
		range: 1
	},
	{
		name: 'succulente',
		category: 'typGreas',
		id: '9vn',
		light: 2,
		water: 1,
		price: 10,
		cover: succulent,
		range: 3
	},

	{
		name: 'menthe',
		category: 'typOut',
		id: '6uo',
		light: 2,
		water: 2,
		price: 8,
		cover: mint,
		range: 3
	}
]