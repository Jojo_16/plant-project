import { useState, useEffect } from 'react'
import '../styles/Cart.scss'

function Cart({ cart, updateCart }) {
	const [isOpen, setIsOpen] = useState(true)
	const total = cart.reduce(
		(acc, plantType) => acc + plantType.amount * plantType.price,
			0
		);
	useEffect(() => {
		document.title = `LMJ: ${total}€ d'achats 💸`
	}, [total]);

	// useEffect(() => {
	// 	alert('Bienvenue dans la maison jungle')
	// }, []);

	function deleteItem(name) {
		const currentPlantSaved = cart.find((plant) => plant.name === name)
		
		if(currentPlantSaved.amount > 1) {
			currentPlantSaved.amount -= 1;
		} else {
			cart.splice(cart.indexOf(currentPlantSaved), 1);
		}

		updateCart([...cart]);
	}

	return isOpen ? (
		<div className='lmj-cart'>
			<button
				className='btn btn-secondary lmj-cart-toggle-button'
				onClick={() => setIsOpen(false)}
			>
				Fermer
			</button>
			{cart.length > 0 ? (
				<div>
					<h2>Panier</h2>
					<div>
						{cart.map(({ name, price, amount }, index) => (
							<div className='article-item' key={`${name}-${index}`}>
								<button 
									className='btn btn-secondary lmj-cart-toggle-button' 
									onClick={() => deleteItem(name)}
									>X</button>
									<div>
										<h4 className='big-title'>{name}</h4>
										<p>Quantité : {amount}</p>
										<p>Prix total : {price * amount}€</p> 
									</div>
							</div>
						))}
					</div>
					<h3>Total :{total}€</h3>
					<div className='align-center'>
						<button 
							className='btn btn-secondary lmj-cart-toggle-button' 
							onClick={() => updateCart([])}
						>Vider le panier</button>
					</div>
				</div>
			) : (
				<div>Votre panier est vide</div>
			)}
		</div>
	) : (
		<div className='lmj-cart-closed'>
			<button
				className='btn-primary lmj-cart-toggle-button'
				onClick={() => setIsOpen(true)}
			>
				Ouvrir le Panier
			</button>
		</div>
	)
}

export default Cart