import { useState } from 'react'
import '../styles/Footer.scss'

function Footer() {
	const [inputValue, setInputValue] = useState('')

  function onBlur() {
    if (!inputValue.includes('@')) {
      alert("Attention, il n'y a pas d'@, ceci n'est pas une adresse valide.")
    } else {
      alert("Merci de nous avoir laissé votre mail !")
    }
  }

  function onChange(e) {
    setInputValue(e.target.value);
  }

	return (
		<footer className='lmj-footer'>
			<div className='lmj-footer-elem'>
				Pour les passionné·e·s de plantes 🌿🌱🌵
			</div>
			<div className='lmj-footer-elem'>
        Laissez-nous votre mail : 
        <input 
          placeholder='Insérez votre mail'
          value={inputValue} 
          onChange={onChange} 
          onBlur={onBlur}
        ></input>
      </div>
		</footer>
	)
}

export default Footer