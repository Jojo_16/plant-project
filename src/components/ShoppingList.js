import { plantList } from '../datas/plantList'
import '../styles/ShoppingList.scss'
import PlantItem from './PlantItem'
import Categories from './Categories'
import { useState } from 'react'

function ShoppingList({ cart, updateCart, setModalIsOpen, setCurrentPlant }) {
	const [cat, setCat] = useState('');
	const [filters, setFilters] = useState({category: [], water: [], light: []});

	function addToCart(name, price) {
		const currentPlantSaved = cart.find((plant) => plant.name === name)
		if (currentPlantSaved) {
			currentPlantSaved.amount += 1;
			updateCart([...cart]);
		} else {
			updateCart([...cart, { name, price, amount: 1 }])
		}
	}

	function checkFilter(type, value) { 
		return filters[type].indexOf(value) !== -1 || filters[type].length === 0;
	}

	return (
		<div className='lmj-shopping-list'>
			<Categories cat={cat} setCat={setCat} filters={filters} setFilters={setFilters}/>
			<ul className='lmj-plant-list'>
				{plantList.map((plantItem) =>
					checkFilter('category', plantItem.category) && checkFilter('water', plantItem.water) && checkFilter('light', plantItem.light) ? (
					<div className='align-center' key={plantItem.id}>
						<PlantItem
							plantItem={plantItem}
							setModalIsOpen={setModalIsOpen}
							setCurrentPlant={setCurrentPlant}
						/>
						<button className='btn btn-primary'onClick={() => addToCart(plantItem.name, plantItem.price)}>Ajouter</button>
					</div>
				): null)}
			</ul>
		</div>
	)
}
  
export default ShoppingList