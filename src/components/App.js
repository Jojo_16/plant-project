import { useEffect, useState } from 'react';
import Banner from './Banner';
import logo from '../assets/logo.png';
import Cart from './Cart';
import Footer from './Footer';
import Modal from "react-modal";
import ShoppingList from './ShoppingList';
import CareScale from './CareScale';
import '../styles/Layout.scss';

function App() {
	const savedCart = localStorage.getItem('cart');
	const [cart, updateCart] = useState(savedCart ? JSON.parse(savedCart) : []);
	const [modalIsOpen, setModalIsOpen] = useState(false);
	const [currentPlant, setCurrentPlant] = useState();
	
	const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };
  
  function closeModal() {
    setModalIsOpen(false);
  }

	useEffect(() => {
		localStorage.setItem('cart', JSON.stringify(cart))
	}, [cart]);

	return (
		<div>
			<Banner>
				<img src={logo} alt='La maison jungle' className='lmj-logo' />
				<h1 className='lmj-title'>La maison jungle</h1>
			</Banner>
			<div className='lmj-layout-inner'>
				<Cart cart={cart} updateCart={updateCart} />
				<ShoppingList 
					cart={cart} 
					updateCart={updateCart}
					setModalIsOpen={setModalIsOpen}
					setCurrentPlant={setCurrentPlant}
				/>
				<Modal
					isOpen={modalIsOpen}
					onRequestClose={closeModal}
					style={customStyles}
					aria-labelledby="modal-modal-title"
					aria-describedby="modal-modal-description"
					ariaHideApp={false}
				>
					<button onClick={closeModal}>Fermer</button>
					<div>
						<h2>{currentPlant ? currentPlant.name : ''}</h2>
						<p>Description de cette plante...</p>
						<div>
							<h4>Besoin de cette plante :</h4>
							<CareScale careType='water' scaleValue={currentPlant ? currentPlant.water : 0} />
							<CareScale careType='light' scaleValue={currentPlant ? currentPlant.light : 0} />
						</div>
					</div>
				</Modal>
			</div>
			<Footer />
		</div>
	)
}

export default App