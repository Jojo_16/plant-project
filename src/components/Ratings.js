import { useState } from 'react';
import '../styles/Ratings.scss';


function Ratings({ plantItem }) {
  const [inputValue, setInputValue] = useState('');
  const [range, setRange] = useState(plantItem.range);
  
  function changeValue(e) {
    setInputValue(e.target.value)
  }

  function addRate() {

    const numberValue = Number(inputValue);
    if(numberValue >= 0 && numberValue < 6) {
      setRange((range + numberValue) / 2);
    } else {
      alert("Merci d'insérer un chiffre en 0 et 5.")
    }
    setInputValue('');
  }

  return (
    <div>
      <h5>{range}/5</h5>
      <form className='rating-block'>
        <input 
          name='rate'
          type='number'
          required
          max='5'
          value={inputValue}
          onChange={changeValue}
          placeholder='Notez la plante'
        />
        <button type="button" className="btn btn-secondary" onClick={() => addRate()}>Noter</button>
      </form>
    </div>
)};

export default Ratings