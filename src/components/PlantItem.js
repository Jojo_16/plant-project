import '../styles/PlantItem.scss';
import Ratings from './Ratings';


function PlantItem({ plantItem, setModalIsOpen, setCurrentPlant }) {
  
  function openModal() {
    setCurrentPlant(plantItem);
    setModalIsOpen(true);
  }

  return (
    <li className='lmj-plant-item'>
      <img 
        className='lmj-plant-item-cover' 
        src={plantItem.cover} 
        alt={plantItem.name}
      />
      <h4>{plantItem.name}</h4>
      <Ratings 
        plantItem={plantItem}
      />
      <button className="btn btn-secondary" onClick={() => openModal()}>+ d'infos</button>
    </li>
)};

export default PlantItem