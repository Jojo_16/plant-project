import '../styles/Categories.scss'

function Categories({ filters, setFilters }) {
  const filterInputs = [{
    label: "Types de plantes",
    name: 'category',
    filters: [{
      value: 'typOut',
      label: "Plantes d'extérieur"
    }, {
      value: 'typGreas',
      label: "Plantes grasses"
    }, {
      value: 'typClass',
      label: "Plantes classiques"
    }] 
   }, {
    label: "Besoin en eau",
    name: 'water',
    filters: [{
      value: 1,
      label: "Peu"
    }, {
      value: 2,
      label: "Modéremment"
    }, {
      value: 3,
      label: "Abondamment"
    }]
   }, {
    label: "Ensoleillement",
    name: 'light',
    filters: [{
      value: 1,
      label: "Peu"
    }, {
      value: 2,
      label: "Modéremment"
    }, {
      value: 3,
      label: "Abondamment"
    }]
   }];

  function changeInput(value, filterType) {
    const currentFilter = filters[filterType];
    const indexOfFilter = currentFilter.indexOf(value);
    if(indexOfFilter !== -1) {
      currentFilter.splice(indexOfFilter, 1);
    } else {
      currentFilter.push(value);
    }
    setFilters({...filters});
  }


  return (
    <div>
      <h2>Filtres</h2>
      <div className="container">
        {filterInputs.map((inputGroup, index) => (
          <div className="filter-block" key={index}>
            <h3>{inputGroup.label}</h3>
            {inputGroup.filters.map(({value, label}, index) => (
              <div key={'input-'+index}>
                <input 
                  id={index + '-' + value + inputGroup.label} 
                  type="checkbox" 
                  name={inputGroup.name} 
                  value={value} 
                  onChange={(e) => changeInput(value, inputGroup.name)}/>
                <label htmlFor={index + '-' + value + inputGroup.label}>{label}</label>
              </div>
            ))}
          </div>
        ))}
      </div>
    </div>
  )
}

export default Categories